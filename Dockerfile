ARG VERSION=alpine

FROM google/cloud-sdk:${VERSION}

COPY install.sh /root/
RUN /bin/bash /root/install.sh
