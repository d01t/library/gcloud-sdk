#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
apk add rsync openssh openssl make git sed
